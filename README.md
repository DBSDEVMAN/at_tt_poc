# Proof of Concept #

## Abbie Tolon & Taylor Thomas ##

We utilized data from the Spotify Top 100 playlists for every year between 2011 and 2019. The link to the data source is below.

*Link to data source:* https://www.kaggle.com/datasets/muhmores/spotify-top-100-songs-of-20152019

The following repo details our process for profiling the data and assessing its quality, cleaning the data, performing the ETL, and then answering business questions. 

Below, we have a summary of our data model, the tables from our profile, the visuals from our business questions, and other high-level summaries. 

For more detailed information, please reference the related files in our repo. 

## Spotify Data Model ##
![test](/Spotify_data_model.png)

## Spotify Data Profile ##


![test](/data_profile/spotify_data_summary.png)


![test](/data_profile/spotify_num_summary.png)


![test](/data_profile/spotify_obj_summary.png)


![test](/data_profile/top_5_summary.png)



## Spotify Data Assessment ##

Overall, the data is high quality and requires only minimal cleaning. There are no *actual* missing values. However, the csv file imported 3 extra blank rows, so we need to remove them. We have 17 features, 5 of which are object type, and the rest are floats. 

Here are some of the following data cleaning steps we will take: 

- Convert the "year released" and "top year" features to be an integer, rather than a float
- Remove unnecessary columns (added) because that doesn't add anything to our analysis
- Remove the spaces from column names and replace with underscore
- Remove the commas from "artist" and "title" fields
- Create a usable song_ID variable

## Spotify Business Questions ##

** Which year had the most popular songs? **

![test](/data_vis/boxplot_year_pop.png)



** Who were the top 10 artists overall? **

![test](/data_vis/lolipop_most_popular.png)


** Has the average song duration decreased over time? **

![test](/data_vis/lineplot_average_song.png)


